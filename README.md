# polybalarm

![](example.gif)

Simple perl script & config file combo to show upcoming events in a Polybar module.



Polybar module setup:
```
[module/polybalarm]
type = custom/script
exec = perl ~/.config/polybar/scripts/polybalarm.pl print X
tail = true
click-left = perl ~/.config/polybar/scripts/polybalarm.pl next
click-right = perl ~/.config/polybar/scripts/polybalarm.pl refresh
```

Multiple events in alarm range will be printed every print-tick (`X` seconds)

Config file (default: ~/.polybalarms):
```
2021-02-27T15:00 7d Test1
2021-03-01T15:00 1h! Test2
2021-03-01T15:00 1d+10m!+5m!! Test3
```

 Examples:
- Entry 1: 7 days before the specified date the script will print Test1'
- Entry 2: 1h before the date the script will print 'Test2' in red (default)
- Entry 3: 1 day before the date the script will print 'Test 3', changes to red 10 minutes before and also prints a notification 5 minutes before
	           
Timers can be chained if you want multiple timers per event.

You can also use `polybalarm.pl add [date] [alarm] [text]` to add events.

Entry format: Alarm date, notification starts X units (Y,M,d,h,m) before the event with custom text. A '!' notes an urgent event with custom output format (default: red text color -- [but all Polybar format options are possible](https://github.com/polybar/polybar/wiki/Formatting#format-tags))


## todo / ideas

- [x] use `notify-send` for urgency alternatives (e.g. when an event is only minutes away)
- [x] add click event to show next event(s) regardless of alarm timer
- [ ] add possibility to directly import from CalDav (or even regular live updates...) -- but maybe this is too much for the purpose of the script
