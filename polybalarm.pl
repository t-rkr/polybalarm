#!/usr/bin/perl

use warnings;
use strict;
use Time::Local;
STDOUT->autoflush(1);

###############
# DESCRIPTION
#

my $DESCRIPTION = "
 Simple perl script & config file combo to
 show upcoming events in a Polybar module.

 Usage:

 Add polybar module:
	[module/polybalarm]
	type = custom/script
	exec = perl ~/.config/polybar/scripts/polybalarm.pl print X
	tail = true
	click-left = perl ~/.config/polybar/scripts/polybalarm.pl next
	click-right = perl ~/.config/polybar/scripts/polybalarm.pl refresh

 Multiple events in alarm range will be printed every print-tick
 (X seconds)

 Add config file (Default: ~/.polybalarms)
	2021-02-27T15:00 7d Test1
	2021-03-01T15:00 1h! Test2
	2021-03-01T15:00 1d+10m!+5m!! Test3

 Examples:
	- Entry 1: 7 days before the specified date the script will print 'Test1'
	- Entry 2: 1h before the date the script will print 'Test2' in red (default)
	- Entry 3: 1 day before the date the script will print 'Test 3', changes to
	           red 10 minutes before and also prints a notification 5 minutes before

	Timers can be chained if you want multiple timers per event

 You can also use `polybalarm.pl add [date] [alarm] [text]` to add events.

 Keywords:

 print X: Start continuous output
 add [date] [alarm] [text]: To add event to config file
 next: Prints next event, regardless of alarm timer
 refresh: Re-read config file

 Changelog:
";

my @VERSION = (
"v02-03-2021",
"v02-03-2021",
"v01-03-2021",
"v28-02-2021",
"v27-02-2021"
);

my @CHANGELOG = (
"if nothing is printed, change delay to at least 60sec",
"added notify-send support + chained timers",
"all recurring I/O is now done from the tmpDir; added refresh function",
"changed to continuous output",
"initial"
);

# Do not change the order of the entries!
# Always add new scripts at the bottom of the array
my @DEPENDENCIES = (
"notify-send"
);

my $LOGO = "
\t####################
\t#                  #
\t#    Polybalarm    #
\t#                  #
\t####################
";

###############
# VARIABLES
#

### MODIFY OPTIONS HERE vvv

# path + name of the config file containing
# the dates
# format:
# YYYY-MM-ddThh:mm X[mhdMY] text
# You can use - or . as date delimiters
my $configFile = "$ENV{HOME}/.polybalarms";

# default print delay in seconds (unless given as argument)
my $delayTime = 10;

# refresh event list every N cycle
my $refreshCycles = 6;

# tmp directory to use for cycling
my $tmpDir = "/tmp";

# urgency modifier
# for options see https://github.com/polybar/polybar/wiki/Formatting#format-tags
my $urgMod = "%{F#f00}";

# default output
my $defaultOutput = "You're free to explore the world :-)";

### MODIFY OPTIONS HERE ^^^

my $mode = "";
my %didNotify = ();

###############
# INPUT
#

&checkDependencies;

if (!$ARGV[0]) {die "\tERROR ($0 - input): No argument given!\n\n";}
elsif ($ARGV[0] eq "-h" || $ARGV[0] eq "-help" || $ARGV[0] eq "--help") {&help;}
else {$mode = $ARGV[0];}

###############
# MAIN
#

if ($mode eq "print") {

	&printEvents;

} elsif ($mode eq "add") {

	&addEvent;

} elsif ($mode eq "next") {

	&printNextEvent;

} elsif ($mode eq "refresh") {

	&prepareRefresh;

}

###########
### SUBROUTINES
##
#

sub prepareRefresh {

	open (my $outFH, ">", "$tmpDir/polybalarm.refresh") or
	die "\n\tERROR ($0 - prepareRefresh): Could not create $tmpDir/polybalarm.refresh!\n\n";

	close ($outFH);

	system("$DEPENDENCIES[0] 'Polybalarm' 'Refresh triggered'");

}

sub readNextEvent {

	open (my $inFH, "<", "$tmpDir/polybalarm.next");

	my $line = <$inFH>;

	print $line;

	close ($inFH);

}

sub printNextEvent {

		(my $printString, my $betterDelta) = &getNextEvent;

		open (my $outFH, ">", "$tmpDir/polybalarm.next") or
		die "\n\tERROR ($0 - printNextEvent): Could not write to $tmpDir/polybalarm.next!\n\n";

		print $outFH "Next event: " . $printString . " in " . $betterDelta . "\n";

		close ($outFH);

}

sub getNextEvent {

	(my $nEvents, my %storedEvents) = &getEvents;

	my $delta = 0;
	my $nextEventIndex = 0;

	for (0 .. $nEvents) {

			my $e = $_;

			if ($e == 0) {$delta = $storedEvents{$e}{delta};}
			elsif ($e > 0 && $storedEvents{$e}{delta} < $delta) {

				$delta = $storedEvents{$e}{delta};
				$nextEventIndex = $e;

			}

	}

	return ($storedEvents{$nextEventIndex}{printString}, $storedEvents{$nextEventIndex}{betterDelta});

}

sub addEvent {

	my @input = @ARGV;
	# remove first entry
	my $trash = shift @input;

	my $alarmDate = shift @input;

	# sanity check for the date
	if ($alarmDate !~ /T/ || $alarmDate !~ /\:/) {
		die "\n\tERROR ($0 - add mode): Your alarm date input seems to be missing the time (YYYY-MM-ddThh:mm)!\n\n";
	}
	if ($alarmDate !~ /-/ && $alarmDate !~ /\./)  {
		die "\n\tERROR ($0 - add mode): Your alarm date seems to be using the wrong delimiter!\n\n";
	}

	my $alarmTimer = shift @input;
	if ($alarmTimer !~ /m|h|d|M|Y/) {
		die "\n\tERROR ($0 - add mode): Your alarm timer seems to be missing a unit!\n\n";
	}

	my @alarmText = @input;
	my $textString = join (" ", @alarmText);

	open (my $outFH, ">>", "$configFile") or
	die "\n\tERROR ($0 - add mode): Could not write to $configFile!\n\n";

	print $outFH "$alarmDate $alarmTimer $textString\n";

	close ($outFH);

	system("cp $configFile $tmpDir");

	system("$DEPENDENCIES[0] 'Polybalarm' 'Added new event'");

}

sub printEvents {

	my $cycle = 0;

	my $oldConfigFile = $configFile;
	system("cp $oldConfigFile $tmpDir");
	# update configFile path
	my @splitPath = split (/\//, $configFile);
	$configFile = $tmpDir . "/" . $splitPath[-1];

	(my $nEvents, my %storedEvents) = &getEvents;

	while() {

		if (-e "$tmpDir/polybalarm.next") {
			&readNextEvent;
			unlink ("$tmpDir/polybalarm.next");

			if ($ARGV[1]) {
				sleep($ARGV[1]);
			} else {
				sleep($delayTime);
			}

		}

		if (($cycle > 0 && $cycle % $refreshCycles == 0) || -e "$tmpDir/polybalarm.refresh") {
			system("cp $oldConfigFile $tmpDir") if (-e "$tmpDir/polybalarm.refresh");
			($nEvents, %storedEvents) = &getEvents;
			unlink ("$tmpDir/polybalarm.refresh") if (-e "$tmpDir/polybalarm.refresh");
			$cycle = 0;
		}

		my $printed = 0;

		for (0 .. $nEvents) {

			my $e = $_;

			if ($storedEvents{$e}{alarmToggled} == 1) {

### MODIFY OUTPUT STRING HERE >>>vvv
				my $notification = $storedEvents{$e}{printString} . " in " . $storedEvents{$e}{betterDelta};
### MODIFY OUTPUT STRING HERE >>>^^^

				if ($storedEvents{$e}{urgentMode} == 0) {
					print $notification . "\n";
				} else {
					print $urgMod . $notification . "\n";
				}

				if ($storedEvents{$e}{urgentMode} == 2 && !$didNotify{$e}) {
					system("$DEPENDENCIES[0] 'Polybalarm' '$notification'");
					$didNotify{$e} = 1;
				}

				if ($ARGV[1]) {
					sleep($ARGV[1]);
				} else {
					sleep($delayTime);
				}

				$printed++;
			}

			if (-e "$tmpDir/polybalarm.next") {
				&readNextEvent;
				unlink ("$tmpDir/polybalarm.next");

				if ($ARGV[1]) {
					sleep($ARGV[1]);
				} else {
					sleep($delayTime);
				}

			}

			if (-e "$tmpDir/polybalarm.refresh") {
				system("cp $oldConfigFile $tmpDir");
				($nEvents, %storedEvents) = &getEvents;
				unlink ("$tmpDir/polybalarm.refresh");
			}

		}

		if ($printed == 0) {
			print $defaultOutput."\n";

			if ($ARGV[1] && $ARGV[1] > 60) {
				sleep($ARGV[1]);
			} elsif ($delayTime > 60) {
				sleep($delayTime);
			} else {
				sleep(60);
			}
		}

		$cycle++;

	}

}

sub getEvents {

	my %events = ();
	my $index = 0;

	my $currentDate = time();

	open (my $inFH, "<", $configFile) or
	die "\n\tERROR ($0 - print): Could not open/find $configFile!\n\n";

	while (my $line = <$inFH>) {

		if ($line =~ /^#/ || $line =~ /^$/) {next;}
		else {

			chomp($line);

			(my $alarmDate, my $alarmTimer, my @alarmText) = split (/\s+/, $line);

			(my $YMd, my $hm) = split (/T/, $alarmDate);

			my $delim = "-";

			if ($YMd !~ /$delim/) {$delim = ".";}

			(my $Y, my $M, my $d) = split (/$delim/, $YMd);
			(my $h, my $m) = split (/\:/, $hm);

			$events{$index}{alarmDate} = timelocal(0,$m,$h,$d,$M-1,$Y);

			my @timers = split (/\+/, $alarmTimer);

			for (0 .. $#timers) {

				my $t = $_;

				$events{$index}{urgentMode} = 0 if ($t == 0);

				$events{$index}{delta} = $events{$index}{alarmDate} - $currentDate;

				my $compareString = $timers[$t];
				$compareString =~ s/!//g;
				my $order = chop $compareString;

				if ($order eq "m") {$compareString *= 60;}
				if ($order eq "h") {$compareString *= 60*60;}
				if ($order eq "d") {$compareString *= 1440*60;}
				if ($order eq "M") {$compareString *= 43800*60;}
				if ($order eq "Y") {$compareString *= 525600*60;}

				$events{$index}{alarmToggled} = 0 if ($t == 0);

				if ($events{$index}{delta} <= $compareString && $events{$index}{delta} > 0) {
					$events{$index}{alarmToggled} = 1;

					if ($timers[$t] =~ /!/) {
						$events{$index}{urgentMode} = $timers[$t] =~ tr/!//;
					}
				}

			}

			$events{$index}{printString} = join (" ", @alarmText);

			$events{$index}{betterDelta} = &getBetterTime($events{$index}{delta});

			$index++;

		}

	}

	close ($inFH);

	$index--;

	return ($index, %events);

}

sub getBetterTime {

	my $delta = shift @_;

	my $years = int($delta/(525600*60));
	$delta = $delta % (525600*60);
	my $months = int($delta/(43800*60));
	$delta = $delta % (43800*60);
	my $days = int($delta/(1440*60));
	$delta = $delta % (1440*60);
	my $hours = int($delta/(60*60));
	$delta = $delta % (60*60);
	my $minutes = int($delta/(60));
	$delta = $delta % (60);
	if ($delta >= 30) {$minutes++};


	if ($years == 0) {
		if ($months == 0) {
			if ($days == 0) {
				if ($hours == 0) {
					return "${minutes}m";
				} else {return "${hours}h ${minutes}m";}
			} else {
				if ($hours == 0) {return "${days}d";}
				else {return "${days}d ${hours}h";}
			}
		} else {
			if ($days == 0) {return "${months}M";}
			else {return "${months}M ${days}d";}
		}
	} else {
		if ($months == 0) {return "${years}Y";}
		else {return "${years}Y ${months}M";}
	}

}

sub writeTmp {

	my $index = shift @_;

	open (my $outFH, ">", "$tmpDir/polybalarm.check") or
	die "\n\tERROR ($0 - writeTmp): Could not write to $tmpDir/polybalarm.check!\n\n";

	print $outFH "$index";

	close ($outFH);

}

sub checkTmp {

	my $index = shift @_;

	if (-e "$tmpDir/polybalarm.check") {
		open (my $inFH, "<", "$tmpDir/polybalarm.check");

		my $line = <$inFH>;
		chomp($line);

		close ($inFH);

		if ($index > $line) {return 1;}
		else {return 0;}

	} else {return 1;}

}

sub checkDependencies {

	for (0..$#DEPENDENCIES) {
		my $testString = `which $DEPENDENCIES[$_] 2>/tmp/error.log`;
		if ($testString eq "") {
			die "\n\tERROR ($0 - checkDependencies): $DEPENDENCIES[$_] was not found in your path!\n\n";
		}
	}

	if (-e "/tmp/error.log") {
		system ("rm /tmp/error.log 2>/tmp/error.log");
	}

}

sub help {

	print $LOGO;

	print $DESCRIPTION;

        for (0..$#VERSION) {
                print "  $VERSION[$_] -- $CHANGELOG[$_]\n";
        }

	if ($#DEPENDENCIES != -1) {
		print "\n Needed files in your PATH:\n";
		for (0..$#DEPENDENCIES) {
			print "  $DEPENDENCIES[$_]\n";
		}
	}

        print "\n";

        die "\n\tPlease re-run script without help keyword\n\n";

}
